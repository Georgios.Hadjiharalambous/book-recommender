# book recommender

Book recommender using different methods such as collaborative filtering, Lift, explicit and implicit factorization models with spotlight (https://maciejkula.github.io/spotlight/).